static volatile char *const ethmac = (char *) 0xC0000000;
static volatile unsigned short *const ethmacstat = (char *) 0xC0002000;

extern int main( void ) {

    int i;
    
    for(;;) {
	static const unsigned frame[ 15 ] = {
	    0xFFFFFFFF, // dest MAC (broadcast)
	    0xA046FFFF, // src/dest MAC
	    0x28630121, // src MAC (46:A0:21:01:63:28)
	    0x01000608, // eth type, hw type
	    0x04060008, // ARP protocol type
	    0xA0460100, // request, src MAC
	    0x28630121, // src MAC
	    0x7B00A8C0, // src IP addr (192.168.0.123)
	    0x00000000,
	    0xA8C00000, // req IP addr (192.168.0.1)
	    0x00000100, // req IP addr
	    0x00000000,
	    0x00000000, // padding
	    0x00000000,
	    0x00000000
	};

	for( i = 0; i < 15; i++ ) {
	    ethmac[ ( i << 4 ) | 0x0 ] = ( frame[ i ] >> 0 ) & 0xFF;
	    ethmac[ ( i << 4 ) | 0x4 ] = ( frame[ i ] >> 8 ) & 0xFF;
	    ethmac[ ( i << 4 ) | 0x8 ] = ( frame[ i ] >> 16 ) & 0xFF;
	    ethmac[ ( i << 4 ) | 0xC ] = ( frame[ i ] >> 24 ) & 0xFF;
	}
	ethmac[ 0xFFF0 ] = 60;

#ifdef TTY
	puts( "ARP request sent." );
#endif
	
	for( i = 0; i < 10000000; i++ )
	    asm volatile( "" );
#ifdef TTY
	if( *ethmacstat ) {
	    show( "Received " );
	    showdec( *ethmacstat );
	    show( " byte frame:" );
	    for( i = 0; i < 20; i++ ) {
		showhex( ethmac[ i << 2 ] );
		putchar( ' ' );
	    }
	    newline();
	    *ethmacstat = 0;
	}
#endif
    }
}
