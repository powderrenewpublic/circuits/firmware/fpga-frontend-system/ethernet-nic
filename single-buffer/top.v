module top( input clk25, output txen, output[ 3:0 ] tx, input txc,
	    input rxdv, input[ 3:0 ] rx, input rxc, input rxer, output mdio,
	    output mdc, input intrp, input crs );

    reg[ 7:0 ] frame[ 0:255 ];

    initial
	$readmemh( "frame.hex", frame );
    
    reg res = 1'b1;
    reg[ 21:0 ] ctr = 22'h01000;

    always @( posedge clk25 ) begin
	ctr <= ctr + 1'b1;
	if( &ctr )
	    res <= 1'b0;
    end
    
    wire addr;
    wire[ 7:0 ] rdata;
    wire[ 7:0 ] wdata;
    wire rw;
    wire ack;
    wire irq;

    assign addr = ctr == 22'd60;
    assign wdata = frame[ ctr[ 7:0 ] ];
    assign rw = ctr <= 22'd60;
    
    ethernet eth( clk25, 1'b1, 1'b1, addr, rdata, wdata, rw, ack, irq,
		  txen, tx, txc, rxdv, rx, rxc, rxer, mdio, mdc, intrp, crs,
		  res );
endmodule
