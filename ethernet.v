// Ethernet NIC MAC logic, with double-buffering receiver.

// Bus interface side (see wishbone spec):
//  clk_i - clock
//  stb_i - strobe (chip select): 0=inactive, 1=active
//  adr_i[] - address:
//      0x000-0x7FF accesses frame buffers
//      0x800-0xFFF status/control:
//          0xC000 - transmitter control (write frame length to start TX)
//          0x8000 - receiver control (write to ack and drop current RX frame)
//          0x8000 - receiver status (yields length of current RX frame,
//                       0 is no frame available)
//
//  dat_o[] - read data (RX buffer or status)
//  dat_i[] - write data (TX buffer or control)
//      NB: lower 8 bits of dat_o/dat_i access frame buffers;
//          upper 8 bits are metadata only
//  we_i - write enable: 0=read, 1=write
//  irq_o - interrupt: 0=idle, 1=interrupt raised (currently unused)
//  rst_i - reset: 0=normal, 1=reset

// PHY interface side (see KSZ8081MNX data sheet):
//  txen - TX enable: 0=inactive, 1=active
//  tx[] - TX data
//  txc - TX clock
//  rxdv - RX data valid: 0=invalid, 1=valid
//  rx[] - RX data
//  rxc - RX clock
//  rxer - RX error
//  mdio - management data
//  mdc - management clock
//  intrp - interrupt
//  crs - carrier sense

module ethernet( input clk_i, input cyc_i, input stb_i, input[ 11:0 ] adr_i,
		 output[ 15:0 ] dat_o, input[ 15:0 ] dat_i, input we_i, 
		 output ack_o, output irq_o,
		 output reg txen, output reg[ 3:0 ] tx, input txc, input rxdv,
		 input[ 3:0 ] rx, input rxc, input rxer, output mdio,
		 output mdc, input intrp, input crs, input rst_i );
    
    // RX
    reg rxwritebuf = 1'b0; // in rxc clock domain
    reg rxwritebufsync = 1'b0; // synch
    reg rxreadbuf = 1'b0; // in clk_i clock domain
    wire[ 11:0 ] rxwritep; // write 4 bits, in rxc clock domain
    wire[ 11:0 ] rxwritepnew;
    wire[ 11:0 ] rxwritepsync; // rxwritep, but synched to clk_i clock domain
    wire rxwritepwe;
    reg[ 1:0 ] rxwriterstate;
    localparam WR_IDLE = 2'b00;
    localparam WR_RECV = 2'b01;
    localparam WR_WAITSTART = 2'b10;
    localparam WR_WAITFINISH = 2'b11;
    reg rxwriterstartsync;
    reg rxwriterfinishsync;
    reg[ 1:0 ] rxreaderstate;
    localparam RD_WAITFRAME = 2'b00;
    localparam RD_READ = 2'b01;
    localparam RD_WAITWRITER = 2'b10;
    reg rxreaderbusysync;
    wire[ 7:0 ] rxbyte0, rxbyte1, rxbyte; // addressed byte for read by CPU
    reg[ 10:0 ] rxsize0, rxsize1; // sizes of rxbuf0, rxbuf1
    
    syncword #( 12 ) rxsync( rxc, rxwritepnew, rxwritep, rxwritepwe,
			     clk_i, rxwritepsync, rst_i );
    
    DP16KB #( .DATA_WIDTH_A( 4 ), .DATA_WIDTH_B( 9 ) )
      rxbuf0( .CLKA( rxc ), .WEA( 1'b1 ),
	      .CEA( rxwriterstate == WR_RECV && rxwritebuf == 1'b0 ),
	      .RSTA( 1'b0 ),
	      .ADA2( rxwritep[ 0 ] ),
              .ADA3( rxwritep[ 1 ] ),
              .ADA4( rxwritep[ 2 ] ),
              .ADA5( rxwritep[ 3 ] ),
              .ADA6( rxwritep[ 4 ] ),
              .ADA7( rxwritep[ 5 ] ),
              .ADA8( rxwritep[ 6 ] ),
              .ADA9( rxwritep[ 7 ] ),
              .ADA10( rxwritep[ 8 ] ),
              .ADA11( rxwritep[ 9 ] ),
              .ADA12( rxwritep[ 10 ] ),
              .ADA13( rxwritep[ 11 ] ),
              .DIA0( rx[ 0 ] ),
              .DIA1( rx[ 1 ] ),
              .DIA2( rx[ 2 ] ),
              .DIA3( rx[ 3 ] ),
              .CLKB( clk_i ), .WEB( 1'b0 ), .CEB( 1'b1 ), 
	      .RSTB( 1'b0 ),
              .ADB3( adr_i[ 0 ] ),
              .ADB4( adr_i[ 1 ] ),
              .ADB5( adr_i[ 2 ] ),
              .ADB6( adr_i[ 3 ] ),
              .ADB7( adr_i[ 4 ] ),
              .ADB8( adr_i[ 5 ] ),
              .ADB9( adr_i[ 6 ] ),
              .ADB10( adr_i[ 7 ] ),
              .ADB11( adr_i[ 8 ] ),
              .ADB12( adr_i[ 9 ] ),
              .ADB13( adr_i[ 10 ] ),
              .DOB0( rxbyte0[ 0 ] ),
              .DOB1( rxbyte0[ 1 ] ),
              .DOB2( rxbyte0[ 2 ] ),
              .DOB3( rxbyte0[ 3 ] ),
              .DOB4( rxbyte0[ 4 ] ),
              .DOB5( rxbyte0[ 5 ] ),
              .DOB6( rxbyte0[ 6 ] ),
              .DOB7( rxbyte0[ 7 ] )
    );
    DP16KB #( .DATA_WIDTH_A( 4 ), .DATA_WIDTH_B( 9 ) )
      rxbuf1( .CLKA( rxc ), .WEA( 1'b1 ),
	      .CEA( rxwriterstate == WR_RECV && rxwritebuf == 1'b1 ),
	      .RSTA( 1'b0 ),
	      .ADA2( rxwritep[ 0 ] ),
              .ADA3( rxwritep[ 1 ] ),
              .ADA4( rxwritep[ 2 ] ),
              .ADA5( rxwritep[ 3 ] ),
              .ADA6( rxwritep[ 4 ] ),
              .ADA7( rxwritep[ 5 ] ),
              .ADA8( rxwritep[ 6 ] ),
              .ADA9( rxwritep[ 7 ] ),
              .ADA10( rxwritep[ 8 ] ),
              .ADA11( rxwritep[ 9 ] ),
              .ADA12( rxwritep[ 10 ] ),
              .ADA13( rxwritep[ 11 ] ),
              .DIA0( rx[ 0 ] ),
              .DIA1( rx[ 1 ] ),
              .DIA2( rx[ 2 ] ),
              .DIA3( rx[ 3 ] ),
              .CLKB( clk_i ), .WEB( 1'b0 ), .CEB( 1'b1 ), 
	      .RSTB( 1'b0 ),
              .ADB3( adr_i[ 0 ] ),
              .ADB4( adr_i[ 1 ] ),
              .ADB5( adr_i[ 2 ] ),
              .ADB6( adr_i[ 3 ] ),
              .ADB7( adr_i[ 4 ] ),
              .ADB8( adr_i[ 5 ] ),
              .ADB9( adr_i[ 6 ] ),
              .ADB10( adr_i[ 7 ] ),
              .ADB11( adr_i[ 8 ] ),
              .ADB12( adr_i[ 9 ] ),
              .ADB13( adr_i[ 10 ] ),
              .DOB0( rxbyte1[ 0 ] ),
              .DOB1( rxbyte1[ 1 ] ),
              .DOB2( rxbyte1[ 2 ] ),
              .DOB3( rxbyte1[ 3 ] ),
              .DOB4( rxbyte1[ 4 ] ),
              .DOB5( rxbyte1[ 5 ] ),
              .DOB6( rxbyte1[ 6 ] ),
              .DOB7( rxbyte1[ 7 ] )
    );        
    assign rxbyte[ 7:0 ] = rxreadbuf ? rxbyte1[ 7:0 ] : rxbyte0[ 7:0 ];

    always @( posedge clk_i ) begin
	rxwritebufsync <= rxwritebuf;
	rxreadbuf <= !rxwritebufsync;
    end
    
    // RX writer:
    always @( posedge rst_i or posedge rxc )
	if( rst_i )
	    rxwriterstate <= WR_IDLE;
	else
	    case( rxwriterstate )
	    WR_IDLE:
		if( rxdv && rx == 4'b1101 )
		    rxwriterstate <= WR_RECV;
	    WR_RECV:
		if( !rxdv )
		    rxwriterstate <= WR_WAITSTART;
	    WR_WAITSTART:
		if( rxreaderbusysync )
		    rxwriterstate <= WR_WAITFINISH;
	    WR_WAITFINISH:
		if( !rxreaderbusysync ) begin
		    rxwriterstate <= WR_IDLE;
		    rxwritebuf <= !rxwritebuf;
		end
	    endcase

    assign rxwritepnew = rst_i ||
			 rxwriterstate == WR_WAITFINISH && !rxreaderbusysync ?
			 12'h000 : rxwritep + 12'b1;
    assign rxwritepwe = rst_i ||
			rxwriterstate == WR_WAITFINISH && !rxreaderbusysync ||
			rxwriterstate == WR_RECV && rxdv;
    
    always @( posedge rxc )
	rxreaderbusysync <= rxreaderstate != RD_WAITFRAME;

    // RX reader:
    always @( posedge rst_i or posedge clk_i )
	if( rst_i )
	    rxreaderstate <= RD_WAITFRAME;
	else
	    case( rxreaderstate )
	    RD_WAITFRAME:
		if( rxwriterstartsync )
		    rxreaderstate <= RD_READ;
	    RD_READ:		
		if( cyc_i && stb_i && adr_i[ 11 ] && adr_i[ 10 ] && we_i )
		    rxreaderstate <= RD_WAITWRITER;		

	    RD_WAITWRITER:
		if( rxwriterfinishsync )
		    rxreaderstate <= RD_WAITFRAME;
	    endcase

    always @( posedge clk_i ) begin
	rxwriterstartsync <= rxwriterstate == WR_WAITSTART;
	rxwriterfinishsync <= rxwriterstate == WR_WAITFINISH;
    end

    always @( posedge clk_i )
	if( !rxreadbuf && cyc_i && stb_i && 
	    adr_i[ 11 ] && !adr_i[ 10 ] && we_i )
	    rxsize0 <= 11'h000;
	else if( rxreadbuf )
	    rxsize0 <= rxwritepsync[ 11:1 ]; // convert nybbles to bytes
    
    always @( posedge clk_i )
	if( rxreadbuf && cyc_i && stb_i && 
	    adr_i[ 11 ] && !adr_i[ 10 ] && we_i )
	    rxsize1 <= 11'h000;
	else if( !rxreadbuf )
	    rxsize1 <= rxwritepsync[ 11:1 ]; // convert nybbles to bytes
    
    // TX
    wire[ 10:0 ] txwritepnew;
    wire[ 10:0 ] txwritepsync; // txsize, but synched to txc clock domain
    wire txwritepwe;
    reg[ 1:0 ] txwriterstate;
    reg txwriterstartsync;
    reg txwriterfinishsync;
    reg[ 11:0 ] txreadp; // read 4 bits
    reg[ 1:0 ] txreaderstate;
    reg txreaderbusysync;
    wire[ 3:0 ] txdata;
    reg[ 10:0 ] txsize;
    reg[ 10:0 ] txsizeout;

    syncword #( 11 ) txsync( clk_i, txwritepnew, txsizeout, txwritepwe,
	      txc, txwritepsync, rst_i );
    
    DP16KD #( .DATA_WIDTH_A( 9 ), .DATA_WIDTH_B( 4 ) )
      txbuf( .CLKA( clk_i ), .WEA( 1'b1 ),
	     .CEA( cyc_i && stb_i && we_i && !adr_i[ 11 ] ),
	     .RSTA( 1'b0 ),
             .ADA3( adr_i[ 0 ] ),
             .ADA4( adr_i[ 1 ] ),
             .ADA5( adr_i[ 2 ] ),
             .ADA6( adr_i[ 3 ] ),
             .ADA7( adr_i[ 4 ] ),
             .ADA8( adr_i[ 5 ] ),
             .ADA9( adr_i[ 6 ] ),
             .ADA10( adr_i[ 7 ] ),
             .ADA11( adr_i[ 8 ] ),
             .ADA12( adr_i[ 9 ] ),
             .ADA13( adr_i[ 10 ] ),
             .DIA0( dat_i[ 0 ] ),
             .DIA1( dat_i[ 1 ] ),
             .DIA2( dat_i[ 2 ] ),
             .DIA3( dat_i[ 3 ] ),
             .DIA4( dat_i[ 4 ] ),
             .DIA5( dat_i[ 5 ] ),
             .DIA6( dat_i[ 6 ] ),
             .DIA7( dat_i[ 7 ] ),
	     .DIA8( 1'b0 ),
             .CLKB( txc ), .WEB( 1'b0 ), .CEB( 1'b1 ),
	     .RSTB( 1'b0 ),
	     .ADB2( txreadp[ 0 ] ),
             .ADB3( txreadp[ 1 ] ),
             .ADB4( txreadp[ 2 ] ),
             .ADB5( txreadp[ 3 ] ),
             .ADB6( txreadp[ 4 ] ),
             .ADB7( txreadp[ 5 ] ),
             .ADB8( txreadp[ 6 ] ),
             .ADB9( txreadp[ 7 ] ),
             .ADB10( txreadp[ 8 ] ),
             .ADB11( txreadp[ 9 ] ),
             .ADB12( txreadp[ 10 ] ),
             .ADB13( txreadp[ 11 ] ),
             .DOB0( txdata[ 0 ] ),
             .DOB1( txdata[ 1 ] ),
             .DOB2( txdata[ 2 ] ),
             .DOB3( txdata[ 3 ] )
    );        

    // TX writer:
    always @( posedge rst_i or posedge clk_i )
	if( rst_i )
	    txwriterstate <= WR_IDLE;
	else
	    case( txwriterstate )
	    WR_IDLE:
		if( cyc_i && stb_i && !adr_i[ 11 ] && we_i )
		    txwriterstate <= WR_RECV;
	    WR_RECV:
		if( cyc_i && stb_i && adr_i[ 11 ] && adr_i[ 10 ] && we_i )
		    txwriterstate <= WR_WAITSTART;
	    WR_WAITSTART:
		if( txreaderbusysync )
		    txwriterstate <= WR_WAITFINISH;
	    WR_WAITFINISH:
		if( !txreaderbusysync )
		    txwriterstate <= WR_IDLE;
	    endcase

    always @( posedge rst_i or posedge clk_i )
	if( rst_i )
	    txsize <= 11'h000;
	else if( cyc_i && stb_i && adr_i[ 11 ] && adr_i[ 10 ] && we_i )
	    txsize <= dat_i[ 11:0 ];
		     
    assign txwritepnew = rst_i ||
			 txwriterstate == WR_WAITFINISH && !txreaderbusysync ?
			 11'h000 : txsize;
    assign txwritepwe = rst_i ||
			txwriterstate == WR_WAITFINISH && !txreaderbusysync ||
			cyc_i && stb_i && !adr_i && we_i;
    
    always @( posedge clk_i )
	txreaderbusysync <= txreaderstate != RD_WAITFRAME;
		       
    // lengths remaining to transmit (0 means finished)
    reg[ 4:0 ] txpreamble_len;
    reg[ 3:0 ] txfcs_len;
    reg[ 31:0 ] crc;
    reg[ 31:0 ] fcs;

    // CRC: must pipeline to compute 4 bits per cycle
    wire[ 31:0 ] crc1, crc2, crc3, crc4;   
    wire crc1s, crc2s, crc3s, crc4s;

    assign crc1s = crc[ 31 ] ^ tx[ 0 ];   
    assign crc1 = crc1s ? { crc[ 30:0 ], 1'b0 } ^ 32'h04C11DB7 :
		  { crc[ 30:0 ], 1'b0 };   
    assign crc2s = crc1[ 31 ] ^ tx[ 1 ];   
    assign crc2 = crc2s ? { crc1[ 30:0 ], 1'b0 } ^ 32'h04C11DB7 :
		  { crc1[ 30:0 ], 1'b0 };
    assign crc3s = crc2[ 31 ] ^ tx[ 2 ];   
    assign crc3 = crc3s ? { crc2[ 30:0 ], 1'b0 } ^ 32'h04C11DB7 :
		  { crc2[ 30:0 ], 1'b0 };
    assign crc4s = crc3[ 31 ] ^ tx[ 3 ];   
    assign crc4 = crc4s ? { crc3[ 30:0 ], 1'b0 } ^ 32'h04C11DB7 :
		  { crc3[ 30:0 ], 1'b0 };

    always @( posedge txc ) begin
	crc <= txen ? crc4 : 32'h13256D64;

	if( txfcs_len[ 3 ] )
	    fcs <= ~{ crc4[0], crc4[1], crc4[2], crc4[3],
		      crc4[4], crc4[5], crc4[6], crc4[7],
		      crc4[8], crc4[9], crc4[10], crc4[11],
		      crc4[12], crc4[13], crc4[14], crc4[15],
		      crc4[16], crc4[17], crc4[18], crc4[19],
		      crc4[20], crc4[21], crc4[22], crc4[23],
		      crc4[24], crc4[25], crc4[26], crc4[27],
		      crc4[28], crc4[29], crc4[30], crc4[31] };
	else
	    fcs <= { 4'hX, fcs[ 31:4 ] };
    end    
    
    // TX reader:
    always @( posedge rst_i or posedge txc )
	if( rst_i ) begin
	    txreadp <= 12'h000;
	    txreaderstate <= RD_WAITFRAME;
	end else begin
	    txen <= 0;
		    
	    case( txreaderstate )
	    RD_WAITFRAME:
		if( txwriterstartsync ) begin
		    txpreamble_len <= 5'd16;
		    txfcs_len <= 4'd8;		    
		    txreaderstate <= RD_READ;
		end
	    RD_READ:
		begin
		    txen <= 1;
		    
		    if( txpreamble_len )
			txpreamble_len <= txpreamble_len - 5'b1;
		    else if( txreadp != { txwritepsync, 1'b0 } )
			txreadp <= txreadp + 12'b1;
		    else if( txfcs_len != 1 )
			txfcs_len <= txfcs_len - 4'b1;
		    else
			txreaderstate <= RD_WAITWRITER;
		end

	    RD_WAITWRITER:
		if( txwriterfinishsync ) begin
		    txreadp <= 12'h000;
		    txreaderstate <= RD_WAITFRAME;
		end
	    endcase	    
	end
    
    always @( posedge txc ) begin
	txwriterstartsync <= txwriterstate == WR_WAITSTART;
	txwriterfinishsync <= txwriterstate == WR_WAITFINISH;
    end

    always @( * )
	if( txpreamble_len )
	    tx <= 4'h5;
	else if( txreadp == 0 )
	    tx <= 4'hD;
	else if( txfcs_len[ 3 ] )
	    tx <= txdata;
	else
	    tx <= fcs[ 3:0 ];
    
    // CPU interface:
    assign dat_o = adr_i[ 11 ] ?
		   ( rxreadbuf ? { 5'h00, rxsize1 } : { 5'h00, rxsize0 } ) :
		   { 8'h00, rxbyte };

    assign ack_o = cyc_i && stb_i;
    
    assign irq_o = 1'b0;
    assign mdio = 1'b1;
    assign mdc = 1'b0;
endmodule

// Synchronised unidirectional transfer (glitchless, but delayed) between
// clock domains, WORD_LEN bits wide.

// Set we high (active) to transfer a word from wnew to rdat: the most recent
// input data will appear on rdat within finite time.  Only valid data
// will be presented at the output, though if wclk is faster than rclk
// and we is asserted rapidly, then the reader is likely to observe a more
// recent value without having seen all previous states.

// Writer side:
//  wclk - writer clock (independent of rclk)
//  wnew - new input data, must be valid if we is asserted
//  wcur - previous input data
//  we - write enable: 0=disable (reuse wcur), 1=enable (transfer wnew)

// Reader side:
//  rclk - reader clock (independent of wclk)
//  rdat - output data (a glitchless copy of some previous wnew when we
//    was asserted)

// Shared:
//  reset - asynchronous reset

module syncword( wclk, wnew, wcur, we, rclk, rdat, reset );
    parameter WORD_LEN = 8;

    input wclk;
    input[ WORD_LEN - 1:0 ] wnew;
    output reg[ WORD_LEN - 1:0 ] wcur;
    input we;
    input rclk;
    output reg[ WORD_LEN - 1:0 ] rdat;
    input reset;

    localparam
	IDLE = 2'b00,
	WAIT = 2'b01,
	WRITE = 2'b10,
	FINISH = 2'b11;
    reg[ 1:0 ] state = IDLE;
    reg[ WORD_LEN - 1:0 ] common;
    
    reg req = 1'b0, rreq_unsync = 1'b0, rreq = 1'b0, push = 1'b0;
    reg ack = 1'b0, wack_unsync = 1'b0, wack = 1'b0;

    always @( posedge wclk or posedge reset ) begin
	if( reset ) begin
	    wcur <= 0;
	    common <= 0;
	    wack_unsync <= 1'b0;
	    wack <= 1'b0;
	    push <= 1'b0;
	    req <= 1'b0;	    
	    state <= IDLE;
	end else begin
	    if( we )
		wcur <= wnew;
	    
	    wack_unsync <= ack;
	    wack <= wack_unsync;

	    if( state == WAIT && wack )
		push <= 1'b0;
	    else if( we )
		push <= 1'b1;

	    case( state )
	    IDLE:
		if( push ) begin
		    req <= 1'b1;
		    state <= WAIT;
		end
	    WAIT:
		if( wack ) begin
		    common <= we ? wnew : wcur;
		    state <= WRITE;
		end
	    WRITE:
		begin
		    req <= 1'b0;
		    state <= FINISH;
		end
	    FINISH:
		if( !wack )
		    state <= IDLE;
	    endcase
	end
    end

    always @( posedge rclk or posedge reset ) begin
	if( reset ) begin
	    rreq_unsync <= 1'b0;
	    rreq <= 1'b0;

	    ack <= 1'b0;
	    
	    rdat <= 0;
	end else begin
	    rreq_unsync <= req;
	    rreq <= rreq_unsync;

	    ack <= rreq;

	    if( ack && !rreq )
		rdat <= common;
	end
    end    
endmodule

// rxreader has 4 states:
// idle, expecting next frame in rx0
// idle, expecting next frame in rx1
// reading rx1, expecting next frame in rx0
// reading rx0, expecting next frame in rx1

// rxwriter has:
// writing next or incomplete frame in rx0 (rx1 is stable)
// writing next or incomplete frame in rx1 (rx0 is stable)
